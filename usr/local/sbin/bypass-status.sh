#!/bin/bash
THIS_BOX_ROLL=$(egrep "^THIS_BOX_ROLL=" /etc/clearbox.conf |awk -F= '{ print $2 }') 
PEER_HOSTNAME=$(egrep "^PEER_HOSTNAME=" /etc/clearbox.conf |awk -F= '{ print $2 }')
PEER_SSH_PORT=$(egrep "^PEER_SSH_PORT=" /etc/clearbox.conf |awk -F= '{ print $2 }') 

#echo $PEER_HOSTNAME
#echo $PEER_SSH_PORT
if [ "$THIS_BOX_ROLL" == "PRIMARY" ]; then
	ssh -p $PEER_SSH_PORT $PEER_HOSTNAME /usr/local/sbin/bypass-status.sh
fi

if [ "$THIS_BOX_ROLL" == "BACKUP" ]; then
	status0=$(cat /sys/class/bypass/g3bp0/bypass)
	status1=$(cat /sys/class/bypass/g3bp1/bypass)

	if [ "$status0" = "n" ]; then bypass0="Not bypassed, backup in control. LED on Backup is GREEN"; fi
	if [ "$status1" = "n" ]; then bypass1="Not bypassed, backup in control. LED on Backup is GREEN"; fi
	if [ "$status0" = "o" ]; then bypass0="Circuits disconnected, no device in control. LED on Backup is BLACK"; fi
	if [ "$status1" = "o" ]; then bypass1="Circuits disconnected, no device in control. LED on Backup is BLACK"; fi
	if [ "$status0" = "b" ]; then bypass0="Bypass active, primary in control. LED on Backup is RED"; fi
	if [ "$status1" = "b" ]; then bypass1="Bypass active, primary in control. LED on Backup is RED"; fi

	echo Bypass 1 - status is $(cat /sys/class/bypass/g3bp0/bypass) - $bypass0
	echo Bypass 2 - status is $(cat /sys/class/bypass/g3bp1/bypass) - $bypass1
fi
