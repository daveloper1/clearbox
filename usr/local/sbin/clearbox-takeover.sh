#!/bin/bash

THIS_BOX_ROLL=$(egrep "^THIS_BOX_ROLL=" /etc/clearbox.conf |awk -F= '{ print $2 }') 
PEER_HOSTNAME=$(egrep "^PEER_HOSTNAME=" /etc/clearbox.conf |awk -F= '{ print $2 }')
PEER_SSH_PORT=$(egrep "^PEER_SSH_PORT=" /etc/clearbox.conf |awk -F= '{ print $2 }') 

if [ "$THIS_BOX_ROLL" == "PRIMARY" ]; then
	ssh -p $PEER_SSH_PORT $PEER_HOSTNAME /usr/local/sbin/primary-takeover.sh
fi

if [ "$THIS_BOX_ROLL" == "BACKUP" ]; then
	/usr/local/sbin/backup-takeover.sh
fi
