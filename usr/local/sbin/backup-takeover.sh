#!/bin/bash

THIS_BOX_ROLL=$(egrep "^THIS_BOX_ROLL=" /etc/clearbox.conf |awk -F= '{ print $2 }')

if [ "$THIS_BOX_ROLL" == "BACKUP" ]; then
	echo n > /sys/class/bypass/g3bp1/bypass && echo n > /sys/class/bypass/g3bp0/bypass
else
        echo You should not run this command on the Primary ClearBOX
fi
