#!/bin/bash
THIS_BOX_ROLL=$(egrep "^THIS_BOX_ROLL=" /etc/clearbox.conf |awk -F= '{ print $2 }') 
PEER_HOSTNAME=$(egrep "^PEER_HOSTNAME=" /etc/clearbox.conf |awk -F= '{ print $2 }')
PEER_SSH_PORT=$(egrep "^PEER_SSH_PORT=" /etc/clearbox.conf |awk -F= '{ print $2 }') 

#echo $PEER_HOSTNAME
#echo $PEER_SSH_PORT

status=""

if [ "$THIS_BOX_ROLL" == "PRIMARY" ]; then
	ssh -p $PEER_SSH_PORT $PEER_HOSTNAME /usr/local/sbin/clearbox-status.sh
fi

if [ "$THIS_BOX_ROLL" == "BACKUP" ]; then
	status0=$(cat /sys/class/bypass/g3bp0/bypass)
	status1=$(cat /sys/class/bypass/g3bp1/bypass)

	if [ "$status0" == "n" ] && [ $status1 == "n" ]; then 
		status="BACKUP"
	fi

        if [ "$status0" == "b" ] && [ $status1 == "b" ]; then 
                status="PRIMARY"
        fi

	if [ "$status" == "" ]; then
		echo ERROR
	else
		echo $status
	fi
fi
